import React, { useState, useEffect } from "react";
import "./App.css";
import { MockData } from "./mock-data/data";
import acceptCarts from "./assets/image/accept-carts.jpg";

const deliveryOption = [
  { key: "key-1", text: "Standard Delivery(Free)" },
  { key: "key-2", text: "Fast Delivery(Paid)" },
  { key: "key-3", text: "Premium Delivery(Free)" },
];

function Dropdown(props) {
  return (
    <>
      <select {...props}>
        {props.options &&
          props.options.map((o) => (
            <option key={o.key} value={o.key}>
              {o.text}
            </option>
          ))}
      </select>
    </>
  );
}

function App() {
  const [data, setData] = useState(MockData);
  const [color, setColor] = useState("");
  const [size, setSize] = useState("");
  // const [subTotal, setSubTotal] = useState();


  /* const calculateSubTotal = useCallback(() => {
    const total = data.reduce((sum, i) => (sum += i.count * i.price), 0)
    setSubTotal(total);
  }, [setSubTotal]); */

  useEffect(() => {
    return function cleanup() {};
  }, []);

  const onChange = (index, val) =>{
    setData(
      data.map((item, i) => (i === index ? { ...item, count: val } : item))
    );
  }

  const CartHeader = () => (
    <div className="header">
      <div className="my-bag-title">My bag</div>
      <div className="reserved-msg">Items are reserved for 60 minutes</div>
    </div>
  );

  const CartTotal = () => (
    <div className="container-right">
      <div className="total">TOTAL</div>
      <div className="sub-total-section">
        <div className="sub-total-label">Sub-total</div>
        <div className="sub-total">{`£ ${data.reduce((sum, i) => (sum += i.count * i.price), 0)}`}</div>
      </div>
      <div className="delivery-section">
        <div className="delivery">Delivery</div>
        <div className="delivery">
          <span>&#9432;</span>
        </div>
      </div>
      <div className="standard-delivery">
        <Dropdown options={deliveryOption} />
      </div>
      <div className="checkout">
        <button>Checkout</button>
      </div>
      <div className="we-accept-section">
        <div className="we-accept-label">We Accept</div>
        <img className="we-accept" src={acceptCarts} alt="we-accept" />
        <div className="discount-code-message">
          {"Get a discount code, Add it in the next step."}
        </div>
      </div>
    </div>
  );

  const SubTotal = () => (
    <div className="sub-total-section">
      <div className="sub-total-label">Sub-total</div>
      <div className="sub-total">{`£ ${data.reduce((sum, i) => (sum += i.count * i.price), 0)}`}</div>
    </div>
  );

  const CartList = ({ data, onChange }) => (
    <>
      {data.map((item, i) => (
        <div className="itemContainer-item" key={i}>
          <div className="image-section">
            <img className="image-item" src={item.imagePath} alt="product-1" />
          </div>
          <div className="info-section">
            <div className="price">{item.price}</div>
            <div className="details">{item.description}</div>
            <div className="item-info">
              <div className="color">
                <select
                  onChange={(e) => setColor(e.target.value)}
                  value={color}
                >
                  {item.colorOptions &&
                    item.colorOptions.map((o) => (
                      <option key={o.key} value={o.key}>
                        {o.text}
                      </option>
                    ))}
                </select>
              </div>
              <div className="size">
                <select onChange={(e) => setSize(e.target.value)} value={size}>
                  {item.sizeOptions &&
                    item.sizeOptions.map((o) => (
                      <option key={o.key} value={o.key}>
                        {o.text}
                      </option>
                    ))}
                </select>
                {/* <Dropdown options={item.sizeOptions} onChange={(e) => setSize(e.target.value)} value={size}/> */}
              </div>
              <div className="quantity">
                <select
                  onChange={(e) => onChange(i, parseInt(e.target.value) || 0)}
                  value={item.count}
                >
                  {item.quantityOptions &&
                    item.quantityOptions.map((o) => (
                      <option key={o.key} value={o.text}>
                        {o.text}
                      </option>
                    ))}
                </select>
                {/* <Dropdown options={item.quantityOptions} onChange={(e) => setQuantity(e.target.value)} value={quantity}/> */}
              </div>
            </div>
            <div className="save-for-later">
              <button>Save for later</button>
            </div>
          </div>
        </div>
      ))}
    </>
  );

  return (
    <div className="wrapper">
      <div className="container-left">
        <CartHeader />
        <CartList data={data} onChange={onChange} />
        <SubTotal />
      </div>
      <CartTotal />
    </div>
  );
}

export default App;
