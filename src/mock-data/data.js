export const MockData = [
  {
    id: 1,
    imagePath:
      "https://tiimg.tistatic.com/fp/1/006/167/full-sleeves-mens-t-shirt-137.jpg",
    price: "24.25",
    count:0,
    description: "Q Crew-Neck T-shirt",
    colorOptions: [
      { key: "key-1", text: "Black" },
      { key: "key-2", text: "White" },
      { key: "key-3", text: "Yellow" },
      { key: "key-4", text: "Red" },
      { key: "key-5", text: "Blue" },
    ],
    sizeOptions: [
      { key: "key-1", text: "S" },
      { key: "key-2", text: "M" },
      { key: "key-3", text: "L" },
      { key: "key-4", text: "XL" },
    ],
    quantityOptions: [
      { key: "key-0", text: "0" },
      { key: "key-1", text: "1" },
      { key: "key-2", text: "2" },
      { key: "key-3", text: "3" },
      { key: "key-4", text: "4" },
      { key: "key-5", text: "5" },
    ],
  } ,
  {
    id: 2,
    imagePath:
      "https://pantaloons.imgix.net/img/app/product/4/458884-3156803.jpg",
    price: "25.55",
    count:0,
    description: "T-shirt with Contrast Stripes",
    colorOptions: [
      { key: "key-1", text: "Yellow" },
      { key: "key-2", text: "White" },
      { key: "key-3", text: "Black" },
      { key: "key-4", text: "Red" },
      { key: "key-5", text: "Blue" },
    ],
    sizeOptions: [
      { key: "key-1", text: "S" },
      { key: "key-2", text: "M" },
      { key: "key-3", text: "L" },
      { key: "key-4", text: "XL" },
    ],
    quantityOptions: [
      { key: "key-0", text: "0" },
      { key: "key-1", text: "1" },
      { key: "key-2", text: "2" },
      { key: "key-3", text: "3" },
      { key: "key-4", text: "4" },
      { key: "key-5", text: "5" },
    ],
  },
  {
    id: 3,
    imagePath:
      "https://images-na.ssl-images-amazon.com/images/I/418QZ6ILnBL.jpg",
    price: "15.55",
    count:0,
    description: "T-shirt with Contrast Color",
    colorOptions: [
      { key: "key-1", text: "Yellow" },
      { key: "key-2", text: "White" },
      { key: "key-3", text: "Black" },
      { key: "key-4", text: "Red" },
      { key: "key-5", text: "Blue" },
    ],
    sizeOptions: [
      { key: "key-1", text: "S" },
      { key: "key-2", text: "M" },
      { key: "key-3", text: "L" },
      { key: "key-4", text: "XL" },
    ],
    quantityOptions: [
      { key: "key-0", text: "0" },
      { key: "key-1", text: "1" },
      { key: "key-2", text: "2" },
      { key: "key-3", text: "3" },
      { key: "key-4", text: "4" },
      { key: "key-5", text: "5" },
    ],
  }, 
];